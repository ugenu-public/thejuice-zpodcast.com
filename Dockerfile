FROM node:latest

RUN npm install serve -g

WORKDIR /app

COPY . /app

CMD [ "serve", "." ]